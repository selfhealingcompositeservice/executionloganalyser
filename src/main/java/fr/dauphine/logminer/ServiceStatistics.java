package fr.dauphine.logminer;

public class ServiceStatistics {
	
	/** The name of the Service */
	private String name; 
	
	/** The number of records in the log corresponding to this service */
	private double frequency;
	
	/** Total of the sum of the QoS multiplied by oldness */
	private double qosXoldness;
	
	/** Total of the sum of the oldness of all records */
	private double oldnessSum;
	
	/** QoS weighed average using oldness */
	private double qosAverage;
	
	/** Availability of the Service*/
	private float availability;
	
	/** Total of the sum of the QoS without oldness */
	private double qosSumRaw;
	
	/** QoS weighed average without oldness */
	private double qosAverageRaw;

	public double getQosAverageRaw() {
		return qosAverageRaw;
	}

	public void setQosAverageRaw(double qosAverageRaw) {
		this.qosAverageRaw = qosAverageRaw;
	}

	public ServiceStatistics(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceStatistics other = (ServiceStatistics) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ServiceStatistics [name=" + name + ", frequency=" + frequency
				+ ", qosXoldness=" + qosXoldness
				+ ", oldnessSum=" + oldnessSum + ", qosAverage=" + qosAverage
				+ ", availability=" + availability + "]";
	}

	public double getQosXoldness() {
		return qosXoldness;
	}

	public void setQosXoldness(double qosXoldess) {
		this.qosXoldness = qosXoldess;
	}

	public double getOldnessSum() {
		return oldnessSum;
	}

	public void setOldnessSum(double oldnessSum) {
		this.oldnessSum = oldnessSum;
	}

	public double getQosAverage() {
		return qosAverage;
	}

	public void setQosAverage(double qosAverage) {
		this.qosAverage = qosAverage;
	}

	public double getAvailability() {
		return availability;
	}

	public void setAvailability(float availability) {
		this.availability = availability;
	}

	public double getQosSumRaw() {
		return qosSumRaw;
	}

	public void setQosSumRaw(double qosSumRaw) {
		this.qosSumRaw = qosSumRaw;
	}

}
