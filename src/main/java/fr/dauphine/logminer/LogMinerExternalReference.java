package fr.dauphine.logminer;

import java.sql.Timestamp;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Days;

import fr.dauphine.file.LogReaderExternalReference;

public class LogMinerExternalReference {

	/** Each Service with the number of times it appears */
	Map<String, Integer> ocurrences;

	/** Each Service with the number of times it was successful */
	Map<String, Integer> success;

	/** Services analysis results */
	Map<String, ServiceStatistics> stats;

	/** Minimum execution time */
	double minExecutionTime = 0.0;

	/** Maximum execution time */
	double maxExecutionTime = Double.POSITIVE_INFINITY;

	/** Minimum timestamp */
	Timestamp minTimeStamp;

	/** Maximum timestamp */
	Timestamp maxTimeStamp = new Timestamp(Long.MAX_VALUE);

	/** Minimum ocurrences */
	int minOcurrence = 0;

	/** Maximum timestamp */
	int maxOcurrence = (int) Double.POSITIVE_INFINITY;

	/** Scaling minimum and maximum values */
	final static double minNormamization = 0.1;
	final static double maxNormamization = 0.9;

	public LogMinerExternalReference(String filename, double maxExecutionTime,
			Timestamp current, Timestamp maxTimeStamp) {

		this.maxExecutionTime = maxExecutionTime;
		this.minTimeStamp = new Timestamp(new java.util.Date().getTime());
		this.maxTimeStamp = maxTimeStamp;
		this.minTimeStamp = current;
		this.maxOcurrence = Math.abs(Days.daysBetween(
				new DateTime(this.minTimeStamp),
				new DateTime(this.maxTimeStamp)).getDays());

		LogReaderExternalReference.readLog(filename, minExecutionTime,
				maxExecutionTime, minTimeStamp, maxTimeStamp, minOcurrence,
				maxOcurrence);
		stats = LogReaderExternalReference.stats;
	}

	public Map<String, ServiceStatistics> getServiceStatistics() {
		return stats;
	}
}
