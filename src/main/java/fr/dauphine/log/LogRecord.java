package fr.dauphine.log;

import java.sql.Timestamp;

public class LogRecord {

	private String serviceName;
	private double executionTime;
	private double executionTimeNormalized;
	private boolean success;
	private Timestamp timeStamp;
	private double oldness;

	public LogRecord(String serviceName, double executionTime, boolean success,
			Timestamp timeStamp) {
		super();
		this.serviceName = serviceName;
		this.executionTime = executionTime;
		this.success = success;
		this.timeStamp = timeStamp;
	}

	public String getServiceName() {
		return serviceName;
	}

	public double getExecutionTime() {
		return executionTime;
	}

	public boolean isSuccess() {
		return success;
	}

	public Timestamp getTimeStamp() {
		return timeStamp;
	}

	public double getOldness() {
		return oldness;
	}

	public void setOldness(double oldness) {
		this.oldness = oldness;
	}

	public double getExecutionTimeNormalized() {
		return executionTimeNormalized;
	}

	public void setExecutionTimeNormalized(double executionTimeNormalized) {
		this.executionTimeNormalized = executionTimeNormalized;
	}

	@Override
	public String toString() {
		return "LogRecord [serviceName=" + serviceName + ", executionTime="
				+ executionTime + ", executionTimeNormalized="
				+ executionTimeNormalized + ", success=" + success
				+ ", timeStamp=" + timeStamp + ", oldness=" + oldness + "]";
	}

}