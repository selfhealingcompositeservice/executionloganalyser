package fr.dauphine.test;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.common.base.Stopwatch;

import fr.dauphine.logminer.LogMinerExternalReference;

public class TestLogMinerExternalReference {

	public static void main(String... args) {
		System.out.println("Analysing execution log...");
		Stopwatch stopwatch = Stopwatch.createStarted();
		LogMinerExternalReference miner = new LogMinerExternalReference(
				"wsDreamLog2190", 99999.0, stringToTimestamp(
						"2019-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss"),
				stringToTimestamp("2013-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss"));

		stopwatch.stop();

		long millis = stopwatch
				.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS);

		System.out.println("wsDreamLog2190 analysed in " + millis + " ms");
		System.out.println("Number of services: "
				+ miner.getServiceStatistics().size());
		System.out.println(miner.getServiceStatistics());
	}

	public static Timestamp stringToTimestamp(String datestring, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = dateFormat.parse(datestring);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Timestamp(date.getTime());
	}
}
