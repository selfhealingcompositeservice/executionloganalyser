package fr.dauphine.file;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import fr.dauphine.log.LogRecord;
import fr.dauphine.logminer.ServiceStatistics;

public class LogReaderExternalReference {

	final static Charset ENCODING = StandardCharsets.UTF_8;

	static Map<String, Integer> ocurrences;
	static Map<String, Integer> success;
	public static Map<String, ServiceStatistics> stats;

	final static double minNormamization = 0.1;
	final static double maxNormamization = 0.9;

	public static List<LogRecord> readLog(String filename,
			double minExecutionTime, double maxExecutionTime,
			Timestamp minTimeStamp, Timestamp maxTimeStamp, int minOcurrence,
			int maxOcurrence) {

		ocurrences = new HashMap<String, Integer>();
		success = new HashMap<String, Integer>();
		stats = new HashMap<>();

		List<LogRecord> records = new ArrayList<LogRecord>();

		try (Scanner scanner = new Scanner(
				LogRecord.class.getResourceAsStream(filename), ENCODING.name())) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] serviceData = line.split(", ");

				SimpleDateFormat sdf = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				Date date = null;
				try {
					date = sdf.parse(serviceData[3].substring(1,
							serviceData[3].length() - 1));
				} catch (Exception e) {
					e.printStackTrace();
				}

				LogRecord r = new LogRecord(serviceData[0],
						Double.parseDouble(serviceData[1]),
						getSuccess(serviceData[2]), new Timestamp(
								date.getTime()));

				// ocurrences
				if (ocurrences.get(r.getServiceName()) == null)
					ocurrences.put(r.getServiceName(), 1);
				else
					ocurrences.put(r.getServiceName(),
							ocurrences.get(r.getServiceName()) + 1);

				// successful executions
				if (r.isSuccess()) {
					if (success.get(r.getServiceName()) == null)
						success.put(r.getServiceName(), 1);
					else
						success.put(r.getServiceName(),
								success.get(r.getServiceName()) + 1);
				} else if (success.get(r.getServiceName()) == null)
					success.put(r.getServiceName(), 0);

				/* normalizations */
				// oldness
				r.setOldness(scale(normalize(r.getTimeStamp().getTime(),
						minTimeStamp.getTime(), maxTimeStamp.getTime())));
				// execution time
				r.setExecutionTimeNormalized(scale(normalize(
						r.getExecutionTime(), minExecutionTime,
						maxExecutionTime)));

				// qosavg
				ServiceStatistics ss = stats.get(r.getServiceName());
				if (ss == null) {
					ss = new ServiceStatistics(r.getServiceName());
					stats.put(ss.getName(), ss);
				}

				// set numerator of QoSAvg
				ss.setQosXoldness(ss.getQosXoldness() + getQos(r)
						* r.getOldness());
				// set denominator of QoSAvg
				ss.setOldnessSum(ss.getOldnessSum() + r.getOldness());

				records.add(r);
			}

		}

		// results
		for (String ssName : stats.keySet()) {
			ServiceStatistics ss = stats.get(ssName);
			ss.setAvailability((float) success.get(ss.getName())
					/ ocurrences.get(ss.getName()));
			ss.setFrequency(scale(normalize(ocurrences.get(ss.getName()),
					maxOcurrence, minOcurrence)));
			ss.setQosAverage(ss.getQosXoldness() / ss.getOldnessSum());
		}

		return records;
	}

	private static boolean getSuccess(String serviceData) {
		if (serviceData.equals("OK"))
			return true;
		return false;
	}

	private static double getQos(LogRecord r) {
		return r.getExecutionTimeNormalized();
	}

	/**
	 * Returns a normalized value between a range [min,max] (value - min) / (max
	 * - min)
	 * 
	 * @param the
	 *            value to be scaled
	 * @param the
	 *            max value for the normalization
	 * @param the
	 *            min value for the normalization
	 * @return a scaled value between a range [min,max]
	 */
	private static double normalize(double d, double max, double min) {
		if (max == min)
			// If max is equal to ùin then Normalized is set to 0.5.
			return 0.5;
		return (d - min) / (max - min);

	}

	/**
	 * Returns a scaled value between a range [min,max] (value*(max-min) + min
	 * 
	 * @param the
	 *            value to be scaled
	 * @return a scaled value between a range [min,max]
	 */
	private static double scale(double d) {
		return (d * (maxNormamization - minNormamization)) + minNormamization;
	}

}
